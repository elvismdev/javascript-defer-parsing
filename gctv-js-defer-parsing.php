<?php
/**
 * Plugin Name: GCTV JavaScript Defer Parsing
 * Plugin URI: https://bitbucket.org/grantcardone/gctv-javascript-defer-parsing
 * Description: Implements Defer Parsing technique on JavaScript files.
 * Version: 1.0
 * Author: Elvis Morales
 * Author URI: https://twitter.com/n3rdh4ck3r
 * Requires at least: 3.5
 * Tested up to: 4.3
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/* Defer Parsing */
if ( !( is_admin() ) ) {

	add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );
	function defer_parsing_of_js( $url ) {

		// Define here scripts to exclude from defer parsing.
		$skip_js = array( 'jquery.js',
			'livefyre.js',
			'Widgets.js',
			'jquery.themepunch.tools.min.js',
			'jquery.themepunch.revolution.min.js'
			); 

		if ( FALSE === strpos( $url, '.js' ) || strposa( $url, $skip_js ) ) return $url;

		return "$url' defer='defer";
	}

}

function strposa( $haystack, $needle, $offset=0 ) {
	if( !is_array( $needle ) ) $needle = array( $needle );
	foreach( $needle as $query ) {
        if( strpos( $haystack, $query, $offset ) !== false ) return true; // stop on first true result
    }
    return false;
}